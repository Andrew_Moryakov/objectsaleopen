USE [ObjectSaleDB]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertRole]
    @namerole nvarchar(256),
	@id nvarchar(128)
AS    
    INSERT INTO AspNetRoles (Id, Name)  Output Inserted.Id
    VALUES (@id, @namerole)
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_InsertUser]
	@id nvarchar(128),
	@securitystamp nvarchar(max),
    @name nvarchar(max),
    @email nvarchar(256),
	@emailconfirmed bit,
	@phonenumberconfirmed bit, 
	@twofactorenabled bit,
	@lockoutenabled bit,
	@accessfailedcount int,
	@username nvarchar(256),
	@adress nvarchar(max),
	@surname nvarchar(max),
	@passwordhash nvarchar(max)
AS
IF NOT EXISTS (SELECT Email as email FROM AspNetUsers WHERE email = @email)
BEGIN  
    INSERT INTO AspNetUsers(Id,SecurityStamp, Name, Email, EmailConfirmed, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEnabled, AccessFailedCount, UserName, Adress, SurName, PasswordHash) Output Inserted.Id
    VALUES (@id, @securitystamp, @name, @email, @emailconfirmed, @phonenumberconfirmed, @twofactorenabled, @lockoutenabled, @accessfailedcount, @username, @adress, @surname, @passwordhash)
END
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_InsertUserRole]
    @userid nvarchar(128),
	@roleid nvarchar(128)
AS
    INSERT INTO AspNetUserRoles(UserId, RoleId)
    VALUES (@userid, @roleid)
GO