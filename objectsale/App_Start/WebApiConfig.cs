﻿using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Owin.Security.OAuth;

namespace ObjectSale
{
	public static class WebApiConfig
	{
		public static void Register(HttpConfiguration config)
		{
			// Web API configuration and services
			// Configure Web API to use only bearer token authentication.
			config.SuppressDefaultHostAuthentication();
			config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
			//  config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
			// Web API routes
			config.MapHttpAttributeRoutes();
			config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new
			{
				id = RouteParameter.Optional
			});

			config.Routes.MapHttpRoute("ConfirmOrder", "api/{controller}/{action}/{id}", new
			{
				id = RouteParameter.Optional
			});
			//config.Routes.MapHttpRoute(
			//  name: "VisitsGet",
			//  routeTemplate: "api/{controller}/{action}/{from}/{to}"
			// );
			//config.Routes.MapHttpRoute(
			//  name: "GetVisitsOrderByDays",
			//  routeTemplate: "api/{controller}/GetVisitsOrderByDays"
			// );

			//config.Routes.MapHttpRoute("userRoute", "api/{controller}/{Id}/{Name}/{SurName}/{Adress}/{Email}");
			// отключаем возможность вывода данных в формате xml
			config.Formatters.Remove(config.Formatters.XmlFormatter);
			//var json = config.Formatters.JsonFormatter;
			//json.SerializerSettings.PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects;
			//config.Formatters.Remove(config.Formatters.XmlFormatter);
		}
	}
}