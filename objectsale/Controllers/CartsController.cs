﻿//using System.Data.Entity;
//using System.Data.Entity.Infrastructure;
//using System.Linq;
//using System.Net;
//using System.Threading.Tasks;
//using System.Web.Http;
//using System.Web.Http.Description;
//using ObjectSale.Models;

//namespace ObjectSale.Controllers
//{
//	/// <summary>
//	/// 
//	/// </summary>
//	public class CartsController: ApiController
//	{
//		/// <summary>
//		/// 
//		/// </summary>
//		/// <returns></returns>
//		public IQueryable<Order> GetOrders()
//		{
//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				return db.Orders;
//			}
//		}

//		// GET: api/Carts/5
//		[ResponseType(typeof(Order))]
//		public async Task<IHttpActionResult> GetCart(int id)
//		{
//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				Order order = await db.Orders.FindAsync(id);
//				if (order == null)
//				{
//					return NotFound();
//				}
//				return Ok(order);
//			}
//		}

//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="id"></param>
//		/// <param name="order"></param>
//		/// <returns></returns>
//		[ResponseType(typeof(void))]
//		public async Task<IHttpActionResult> PutCart(int id, Order order)
//		{
//			if (!ModelState.IsValid)
//			{
//				return BadRequest(ModelState);
//			}
//			if (id != order.Id)
//			{
//				return BadRequest();
//			}

//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				db.Entry(order).State = EntityState.Modified;
//				try
//				{
//					await db.SaveChangesAsync();
//				}
//				catch (DbUpdateConcurrencyException)
//				{
//					if (!CartExists(id))
//					{
//						return NotFound();
//					}
//					throw;
//				}
//			}

//			return StatusCode(HttpStatusCode.NoContent);
//		}

//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="order"></param>
//		/// <returns></returns>
//		[ResponseType(typeof(Order))]
//		public async Task<IHttpActionResult> PostCart(Order order)
//		{
//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				if (!ModelState.IsValid)
//				{
//					return BadRequest(ModelState);
//				}

//				db.Orders.Add(order);
//				await db.SaveChangesAsync();
//			}

//			return CreatedAtRoute("DefaultApi", new
//			{
//				id = order.Id
//			}, order);
//		}

//		/// <summary>
//		/// 
//		/// </summary>
//		/// <param name="id"></param>
//		/// <returns></returns>
//		[ResponseType(typeof(Order))]
//		public async Task<IHttpActionResult> DeleteCart(int id)
//		{
//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				Order order = await db.Orders.FindAsync(id);

//				if (order == null)
//				{
//					return NotFound();
//				}

//				db.Orders.Remove(order);
//				await db.SaveChangesAsync();

//				return Ok(order);
//			}


//		}

//		protected override void Dispose(bool disposing)
//		{
//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				if (disposing)
//				{
//					db.Dispose();
//				}
//				base.Dispose(disposing);
//			}
//		}

//		private bool CartExists(int id)
//		{
//			using (ApplicationDbContext db = new ApplicationDbContext())
//			{
//				return db.Orders.Count(e => e.Id == id) > 0;
//			}
//		}
//	}
//}