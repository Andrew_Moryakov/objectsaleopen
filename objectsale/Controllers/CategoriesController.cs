﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ObjectSale.Controllers.Helpers;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models;
using ObjectSale.Models.Attributes;

namespace ObjectSale.Controllers
{
	/// <summary>
	/// </summary>
	//[Authorize]
	public class CategoriesController: ApiController
	{
		[AnyExceptionFilter]
		public List<Category> GetCategories()
		{
			CategoryHelper helper = new CategoryHelper();

			return helper.GetCategories();
		}

		//[Authorize(Roles = "employee")]
		[ResponseType(typeof(void))]
		[CheckModelForNull]
		[ValidateModelState(typeof(CategoriesController), "CategoryExists")]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> PutCategory(Category model)
		{
			CategoryHelper helper = new CategoryHelper();
			await helper.EditCategoryAsync(model);

			return StatusCode(HttpStatusCode.NoContent);
		}

		//[Authorize(Roles = "employee")]
		[ResponseType(typeof(Category))]
		[CheckModelForNull]
		[ValidateModelState]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> PostCategory(Category category)
		{
			CategoryHelper helper = new CategoryHelper();
			category = await helper.CreateCategoryAsync(category);

			return CreatedAtRoute("DefaultApi", new
			{
				id = category.Id
			}, category);
		}

		[Authorize(Roles = "employee")]
		[ResponseType(typeof(Category))]
		[CheckModelForNull]
		[ValidateModelState(typeof(CategoriesController), "CategoryExists")]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> DeleteCategory(int id)
		{
			EntityHelper entityHelpers = new EntityHelper();
			CategoryHelper helper = new CategoryHelper();

			List<Product> products = entityHelpers.GetProductsWithThisCategory(id);
			entityHelpers.DeleteConnectThisCategoryWithProducts(products);
			Category category = await helper.DeleteCategoryAsync(id);

			return Ok(category);
		}

		protected override void Dispose(bool disposing)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				if (disposing)
				{
					DbContext.Dispose();
				}
				base.Dispose(disposing);
			}
		}

		private bool CategoryExists(Category category)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				return DbContext.Categories.Find(category.Id) !=null;
			}
		}
	}
}