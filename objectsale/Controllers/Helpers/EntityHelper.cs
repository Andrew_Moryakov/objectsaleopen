﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models;
using ObjectSale.Properties;

namespace ObjectSale.Controllers.Helpers
{
    public class EntityHelper
    {
	    public List<Product> GetProductsWithThisCategory(int categoryId)
	    {
			List<Product> products;
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				products = db.Products.Where(prod => prod.CategoryId == categoryId).ToList();
			}
			return products;
		}

	    public void DeleteConnectThisCategoryWithProducts(List<Product> products)
	    {
			ProductHelper helper = new ProductHelper();
			products.ForEach(el =>
			{
				el.CategoryId = null;
				helper.EditProduct(el);
			});
		}
	}
}