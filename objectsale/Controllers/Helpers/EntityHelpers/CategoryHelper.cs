﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ObjectSale.Models;
using ObjectSale.Properties;

namespace ObjectSale.Controllers.Helpers.EntityHelpers
{
	/// <summary>
	/// Класс для работы с категориями
	/// </summary>
	class CategoryHelper
	{

		#region Async
		/// <summary>
		/// Асинхронно возвращает список категорий
		/// </summary>
		/// <returns>Список категорий</returns>
		public async Task<List<Category>> GetCategoriesAsync()
		{
			return await Task.FromResult(GetCategories());
		}

		/// <summary>
		///     Асинхронно редактирует категорию
		/// </summary>
		/// <param name="category">
		///     Категория с одним или несколькими новыми свойствами, свойство Id должно быть таким же как и у
		///     изменяемой сущности
		/// </param>
		/// <returns>Возвращает объект категории, которая была изменена</returns>
		public async Task<Category> EditCategoryAsync(Category category)
		{
			return await Task.FromResult(EditCategory(category));
		}

		/// <summary>
		/// Асинхронно обавляет категорию в базу данных
		/// </summary>
		/// <param name="category">Категория, которую нужно добавить</param>
		/// <returns>Возвращает объект категории, которая была созданна</returns>
		public async Task<Category> CreateCategoryAsync(Category category)
		{
			return await Task.FromResult(CreateCategory(category));
		}

		/// <summary>
		/// Асинхронно удаляет объект с уканым идентификатором из базы данных
		/// </summary>
		/// <param name="id">Идентификатор категории, которую нужно удалить</param>
		/// <returns>Возвращает объект категории, которая была удалена</returns>
		public async Task<Category> DeleteCategoryAsync(int id)
		{
			return await Task.FromResult(DeleteCategory(id));
		}
		#endregion


		/// <summary>
		/// Возвращает список категорий
		/// </summary>
		/// <returns>Список категорий</returns>
		public List<Category> GetCategories()
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				List<Category> categories = db?.Categories?.ToList();
				return categories;
			}
			//using (ApplicationDbContext dbсContext = new ApplicationDbContext())
			//{
			//	List<Category> categories = dbсContext?.Categories?.ToList();
			//	List<dynamic> categoriesForReturn = new List<dynamic>();

			//	categories?.ForEach(el => categoriesForReturn.Add(new
			//	{
			//		el.Title,
			//		el.Description,
			//		el.Id
			//	}));

			//	return categoriesForReturn;
			//}
		}

		/// <summary>
		///     Редактирует категорию
		/// </summary>
		/// <param name="category">
		///     Категория с одним или несколькими новыми свойствами, свойство Id должно быть таким же как и у
		///     изменяемой сущности
		/// </param>
		/// <returns>Возвращает объект категории, которая была изменен</returns>
		public Category EditCategory(Category category)
		{
			try
			{
				using (var db = new ApplicationDbContext())
				{
					db.Entry(category).State = EntityState.Modified;

					db.SaveChanges();

					return category;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(Settings.Default.CommonDbExRu, ex);
			}
		}

		/// <summary>
		/// Добавляет категорию в базу данных
		/// </summary>
		/// <param name="category">Категория, которую нужно добавить</param>
		/// <returns>Возвращает объект категории, которая была созданна</returns>
		public Category CreateCategory(Category category)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				db.Categories.Add(category);
				db.SaveChanges();

				return category;
			}
		}

		/// <summary>
		/// Удаляет объект с уканым идентификатором из базы данных
		/// </summary>
		/// <param name="id">Идентификатор категории, которую нужно удалить</param>
		/// <returns>Возвращает объект категории, которая была удалена</returns>
		public Category DeleteCategory(int id)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				Category category = db.Categories.Find(id);
				db.Categories.Remove(category);
				db.SaveChanges();

				return category;
			}
		}
	}
}