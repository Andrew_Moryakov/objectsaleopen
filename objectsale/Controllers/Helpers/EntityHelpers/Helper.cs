﻿using System;
using System.IO;

namespace ObjectSale.Controllers.Helpers.EntityHelpers
{
    public class Helper
    {

		/// <summary>
		/// Удаляет файл
		/// </summary>
		/// <param name="pathToFile">Удаляет файл с указанным именем</param>
        public void DeleteFile(string pathToFile)
        {
            try
            {
                if(!string.IsNullOrEmpty(pathToFile) && File.Exists(pathToFile))
                {
                    File.Delete(pathToFile); //Удаляем старый файл 
                }
            }
            catch(Exception ex)
            {
                 throw new Exception(Properties.Settings.Default.IoEx, ex);
            }
        }

		/// <summary>
		/// Записывает 
		/// </summary>
		/// <param name="filebase64"></param>
		/// <param name="path"></param>
        public void WriteBase64ToFile(string filebase64, string pathToSaveFile)
        {
            try
            {
				if (!string.IsNullOrEmpty(filebase64))
				{
					byte[] fileBytes = Convert.FromBase64String(filebase64);//Преобразуем строку в байты

					using (FileStream fs = new FileStream(pathToSaveFile, FileMode.Create))
					{
						fs.Write(fileBytes, 0, fileBytes.Length);//И записываем полученные байты в файл
					}
				}
			}
            catch (Exception ex)
            {
                throw new Exception(Properties.Settings.Default.IoEx, ex);
            }

        }
    }
}
