﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;
using ObjectSale.Models;
using ObjectSale.Models.ViewModels;
using ObjectSale.Properties;

namespace ObjectSale.Controllers.Helpers.EntityHelpers
{
	/// <summary>
	/// Класс содержит методы служащие необходимые для работы с заказами 
	/// </summary>
	class OrderHelper
	{

		#region Async
		/// <summary>
		/// Асинхронно возвращает список всех неподтвержденных заказов
		/// </summary>
		/// <returns>Список неподтвержденных заказов</returns>
		public async Task<List<OrderViewModel>> GetOrdersAsync()
		{
			return await Task.FromResult(GetOrders());
		}

		/// <summary>
		/// Асинхронно записывает список заказов в базу данных
		/// </summary>
		/// <param name="orders">Список заказов</param>
		/// <returns>Возвращает добавленные заказы</returns>
		public async Task<List<Order>> CreateOrderAsync(List<Order> orders)
		{
			return await Task.FromResult(CreateOrder(orders));
		}

		/// <summary>
		/// Асинхронно записывает изменения заказа в базу данных
		/// </summary>
		/// <param name="order">Измененный закз, идентификатор объекта должен соответствовать существующие записи в базе данных</param>
		/// <returns>Возвращает измененную категорию</returns>
		public async Task<Order> EditOrderAsync(Order order)
		{
			return await Task.FromResult(EditOrder(order));
		}

		/// <summary>
		/// Асинхронно удаляет заказ с указанным идентификатором
		/// </summary>
		/// <param name="id">Идентификатор заказа, который необходимо удалить</param>
		/// <returns>Возвращает объект удаленного заказа</returns>
		public async Task<Order> DeleteOrderAsync(int id)
		{
			return await Task.FromResult(DeleteOrder(id));
		}
		#endregion

		/// <summary>
		/// Возвращает список всех неподтвержденных заказов
		/// </summary>
		/// <returns>Список неподтвержденных заказов</returns>
		public List<OrderViewModel> GetOrders()
		{
			using (ApplicationDbContext dbContext = new ApplicationDbContext())
			{
				List<Order> orders = dbContext.Orders.Include(el => el.Product).Where(el => el.Confirmed == false).ToList();
				List<ApplicationUser> users = dbContext.Users.ToList();

				List<OrderViewModel> result = new List<OrderViewModel>();
				foreach (var order in orders)
				{
					OrderViewModel viewOrder = new OrderViewModel(order, users);
					result.Add(viewOrder);
				}

				return result;
			}
		}

		/// <summary>
		/// Подтверждает заказ
		/// </summary>
		/// <param name="id">Идентификатор заказа, который нужно подтвердить</param>
		public void ConfirmOrder(int id)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				Order order = db.Orders.FirstOrDefault(el => el.Id == id);
				if (order != null)
				{
					order.Confirmed = true;
					EditOrder(order);
				}
			}
		}

		/// <summary>
		/// Записывает список заказов в базу данных
		/// </summary>
		/// <param name="orders">Список заказов</param>
		/// <returns>Возвращает добавленные заказы</returns>
		public List<Order> CreateOrder(List<Order> orders)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				foreach (var order in orders)
				{
					var sizes = db.Sizes.Include(el => el.Product).ToList();

					order.SelectedSize = sizes.FirstOrDefault(el => el.SizeСode == order.SelectedSize).SizeСode;
					order.Product = db.Products.FirstOrDefault(el => el.Id == order.ProductId);
					order.UserId = db.Users.FirstOrDefault(el => el.UserName == order.User.UserName)?.Id;

					db.Orders.Add(order);
				}

				db.SaveChanges();

				return orders;
			}
		}

		/// <summary>
		/// Записывает изменения заказа в базу данных
		/// </summary>
		/// <param name="order">Измененный закз, идентификатор объекта должен соответствовать существующие записи в базе данных</param>
		/// <returns>Возвращает измененную категорию</returns>
		public Order EditOrder(Order order)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				Order modifOrder = db.Orders.Include(el=>el.Product).FirstOrDefault(el => el.Id == order.Id);

				if (modifOrder != null)
				{
					modifOrder.CartCount = order.CartCount;
					modifOrder.SelectedSize = order.SelectedSize;
					modifOrder.Confirmed = order.Confirmed;

					db.Entry(modifOrder).State = EntityState.Modified;
					db.SaveChanges();
				}

				return modifOrder;
			}
		} 

		/// <summary>
		/// Удаляет заказ с указанным идентификатором
		/// </summary>
		/// <param name="id">Идентификатор заказа, который необходимо удалить</param>
		/// <returns>Возвращает объект удаленного заказа</returns>
		public Order DeleteOrder(int id)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				Order orderForDelete = db.Orders.Find(id);

				db.Orders.Remove(orderForDelete);
				db.SaveChanges();

				return orderForDelete;
			}
		} 
	}
}