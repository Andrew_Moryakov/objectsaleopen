﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ObjectSale.Models;
using ObjectSale.Models.ViewModels;
using ObjectSale.Properties;

namespace ObjectSale.Controllers.Helpers.EntityHelpers
{
	public class ProductHelper
	{
		#region Async
		/// <summary>
		/// Асинхронно возвращает список товаров
		/// </summary>
		/// <returns>Список категорий</returns>
		public async Task<List<ProductViewModel>> GetProductsAsync()
		{
			return await Task.FromResult(GetProducts());
		}

		/// <summary>
		/// Асинхронно изменяет продукт
		/// </summary>
		/// <param name="product">
		/// Продукт с одним или несколькими новыми свойствами, свойство Id должно быть таким же как и у
		/// изменяемой сущности
		/// </param>
		/// <returns>Возвращает измененный продукт</returns>
		public async Task<Product> EditProductAsync(Product product)
		{
			return await Task.FromResult(EditProduct(product));
		}

		/// <summary>
		/// Асинхронно создает продукт
		/// </summary>
		/// <param name="product">Сущность с параметрами для нового продукта</param>
		/// <returns>Возвращает созданный продукт</returns>
		public async Task<Product> CreateProductAsync(Product product)
		{
			return await Task.FromResult(CreateProduct(product));
		}

		/// <summary>
		/// Асинхронно удаляет из базы данных товар с указанным идентификатором
		/// </summary>
		/// <param name="id">Идентификатор продукта который нужно удалить</param>
		public async Task<Product> DeleteProductAsync(int id)
		{
			return await Task.FromResult(DeleteProduct(id));
		}
		#endregion

		/// <summary>
		/// Асинхронно возвращает список товаров
		/// </summary>
		/// <returns>Список категорий</returns>
		public List<ProductViewModel> GetProducts()
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				List<ProductViewModel> result = new List<ProductViewModel>();
				List<Product> products = db?.Products.Include(el => el.Category).Include(el => el.Sizes).ToList();

				products?.ToList().ForEach(el => result.Add(new ProductViewModel(el)));

				return result;
			}
		}

		/// <summary>
		/// Удаляет из базы данных товар с указанным идентификатором
		/// </summary>
		/// <param name="id">Идентификатор продукта который нужно удалить</param>
		public Product DeleteProduct(int id)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				try
				{
					Product productFromDb = GetProductFromDb(id, db);

					DeleteOldProductImage(productFromDb.ImagePath); //Удаляем связанное с товаром изображение

					db.Products.Remove(productFromDb);
					db.SaveChanges();

					return productFromDb;
				}
				catch (IOException ioEx)
				{
					throw new Exception(Settings.Default.IoEx, ioEx);
				}
				catch (Exception ex)
				{
					throw new Exception(Settings.Default.CommonDbExRu, ex);
				}
			}
		} 

		/// <summary>
		/// Создает продукт
		/// </summary>
		/// <param name="product">Сущность с параметрами для нового продукта</param>
		/// <returns>Возвращает созданный продукт</returns>
		public Product CreateProduct(Product product)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{

				product.ImagePath = OperationProductImage(product);
				product.Sizes = SizesArrayToSizeField(product);

				DbContext.Products.Add(product);
				DbContext.SaveChanges();
			}
			return product;
		}

		/// <summary>
		/// Изменяет продукт
		/// </summary>
		/// <param name="product">
		/// Продукт с одним или несколькими новыми свойствами, свойство Id должно быть таким же как и у
		/// изменяемой сущности
		/// </param>
		/// <returns>Возвращает измененный продукт</returns>
		public Product EditProduct(Product product)
		{
			try
			{
				using (ApplicationDbContext db = new ApplicationDbContext())
				{
					var productForRewrite = GetProductFromDb(product.Id, db);
					var oldSizes = GetSizesCurrentProduct(product.Id, db);

					foreach (var item in oldSizes)
					{ //Удаляем сперва все размеры данного продукта
						db.Sizes.Remove(item);
					}

					productForRewrite.Sizes.Clear(); //Очищаем у продукта цены

					productForRewrite.ImagePath = OperationProductImage(product, productForRewrite);
					productForRewrite.Sizes = SizesArrayToSizeField(product);
					productForRewrite.Title = product.Title;
					productForRewrite.Price = product.Price;
					productForRewrite.Description = product.Description;
					productForRewrite.CategoryId = product.CategoryId;
					productForRewrite.VendorCode = product.VendorCode;


					db.Entry(productForRewrite).State = EntityState.Modified;//Тут ошибка!!!!!!!!!
					db.SaveChanges();

					return productForRewrite;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(Settings.Default.CommonDbExRu, ex);
			}
		}

		#region private methods
		private string OperationProductImage(Product product, Product actualProduct = null)
		{
			if (product.Image == null)
			{
				return null;
			}

			string imagePath = null;
			switch (product.Image.Do)
			{
				case "create":
					imagePath = ToWriteProductImage(product.Image.Name, product.Image.File);
					break;
				case "update":
					imagePath = ReWriteOldProductImage(actualProduct.ImagePath, product.Image.Name,
							product.Image.File);
					break;
				case "delete":
					DeleteOldProductImage(actualProduct.ImagePath);
					break;
				case null:
					imagePath = actualProduct.ImagePath;
					break;
			}

			return imagePath;
		}

		private string ToWriteProductImage(string newProductImageName, string base64)
		{
			Helper helper = new Helper();

			string newProductImagePath = Settings.Default.PathToImages + newProductImageName;
			var newFullImagePath = HttpContext.Current.Server.MapPath("~" + newProductImagePath);

			helper.WriteBase64ToFile(base64, newFullImagePath); //Записываем файл

			return newProductImagePath;
		}

		private string ReWriteOldProductImage(string oldProductImageName, string newProductImageName, string base64)
		{
			DeleteOldProductImage(oldProductImageName);

			string newFullPath = ToWriteProductImage(newProductImageName, base64);
			return newFullPath;
		}

		private void DeleteOldProductImage(string oldProductImageName)
		{
			var oldfullPath = oldProductImageName == null
								  ? null : HttpContext.Current.Server.MapPath("~" + oldProductImageName);
			try
			{
				if (!string.IsNullOrEmpty(oldfullPath) && File.Exists(oldfullPath))
				{
					File.Delete(oldfullPath);
				}
			}
			catch (Exception ex)
			{
				throw new Exception(Settings.Default.IoEx, ex);
			}
		}

		private ICollection<Size> SizesArrayToSizeField(Product product)
		{
			if (product.SizesField != null)
			{
				foreach (var item in product.SizesField)
				{
					product.Sizes.Add(new Size
					{
						SizeСode = item
					});
				}
			}

			return product.Sizes;
		}

		private Product GetProductFromDb(int idProduct, ApplicationDbContext db)
		{
			Product productFromDb = db.Products.Include(el=>el.Sizes).Include(el=>el.Category).FirstOrDefault(el => el.Id == idProduct);
			if (productFromDb == null)
			{
				throw new ArgumentException(Settings.Default.InvalidArguments, nameof(idProduct));
			}
			return productFromDb;
		}

		private List<Size> GetSizesCurrentProduct(int idProduct, ApplicationDbContext db)
		{
			return db.Sizes.Include(el => el.Product).Where(el => el.Product.Id == idProduct).ToList();
		}
		#endregion

	}
}
