﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ObjectSale.Models;
using ObjectSale.Models.ViewModels;
using ObjectSale.Properties;

namespace ObjectSale.Controllers.Helpers.EntityHelpers
{
	/// <summary>
	/// Класс содержит методы для работы с пользователями
	/// </summary>
	public class UserHelper
	{
		/// <summary>
		/// Возвращает список пользователей
		/// </summary>
		/// <returns>Список пользователей</returns>
		public List<UserViewModel> GetUsers()
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				List<ApplicationUser> users = db.Users.ToList();

				List<UserViewModel> reslt = new List<UserViewModel>();
				var roles = db.Roles.ToList();
				users.ForEach(user => reslt.Add(new UserViewModel(user, roles)));

				return reslt;
			}
		}

		/// <summary>
		///     Изменяет пользователя
		/// </summary>
		/// <param name="model">
		///     Пользователь с одним или несколькими новыми свойствами, свойство Id должно быть таким же как и у
		///     изменяемой сущности
		/// </param>
		public ApplicationUser EditUser(RegisterBindingModel model)
		{
				using (var db = new ApplicationDbContext())
				{
					var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
					var user = db.Users.FirstOrDefault(el => el.Id == model.Id);

					user.Name = model.Name;
					user.SurName = model.SurName;
					user.Adress = model.Adress;
					user.Email = model.Email;
					user.UserName = model.Email;

					var oldRoleId = user.Roles?.SingleOrDefault()?.RoleId;


					var oldRoleName = db.Roles.SingleOrDefault(r => r.Id == oldRoleId).Name;
					if (oldRoleName != model.Role)
					{
						userManager.RemoveFromRole(user.Id, oldRoleName);
						userManager.AddToRole(user.Id, model.Role);
					}

					db.Entry(user).State = EntityState.Modified;
					db.SaveChanges();

					return user;
				}
		}

		/// <summary>
		///     Создает нового пользователя
		/// </summary>
		/// <param name="model">Объект с информацией о пользователе</param>
		/// <returns>Результат операции</returns>
		public IdentityResult CreateUser(RegisterBindingModel model)
		{
			try
			{
				using (var db = new ApplicationDbContext())
				{
					var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));
					var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));
					var role1 = new IdentityRole
					{
						Name = model.Role
					};
					//создаем роль
					roleManager.Create(role1);
					//Создаем пользователя
					var admin = new ApplicationUser
					{
						UserName = model.Email,
						Email = model.Email,
						Name = model.Name,
						SurName = model.SurName,
						Adress = model.Adress
					};
					IdentityResult result;
					try
					{
						result = userManager.Create(admin, model.Password);
					}
					catch (Exception ex)
					{
						throw new Exception(Settings.Default.CreateEntityEx + " " + ex.Message, ex);
					}
					if (result.Succeeded)
					{
						userManager.AddToRole(admin.Id, role1.Name);
					}
					return result;
				}
			}
			catch (Exception ex)
			{
				throw new Exception(Settings.Default.CommonDbExRu, ex);
			}
		}

		/// <summary>
		/// Удаляет пользователя
		/// </summary>
		/// <param name="id">Идетфикатор пользователя</param>
		/// <returns>Возвращает удаленного пользователя</returns>
		public ApplicationUser DeleteUser(string id)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				ApplicationUser user = db.Users.Find(id);
				db.Users.Remove(user);
				db.SaveChanges();

				return user;
			}
		}

		#region Async
		/// <summary>
		///     Изменяет пользователя
		/// </summary>
		/// <param name="model">
		///     Пользователь с одним или несколькими новыми свойствами, свойство Id должно быть таким же как и у
		///     изменяемой сущности
		/// </param>
		public async Task<ApplicationUser> EditUserAsync(RegisterBindingModel model)
		{
			return await Task.FromResult(EditUser(model));
		}

		/// <summary>
		/// Удаляет
		/// </summary>
		/// <param name="id">Идетфикатор пользователя</param>
		/// <returns>Возвращает удаленного пользователя</returns>
		public async Task<ApplicationUser> DeleteUserAsync(string id)
		{
			return await Task.FromResult(DeleteUser(id));
		}

		/// <summary>
		///     Создает нового пользователя
		/// </summary>
		/// <param name="model">Объект с информацией о пользователе</param>
		/// <returns>Результат операции</returns>
		public async Task<IdentityResult> CreateUserAsync(RegisterBindingModel model)
		{
			return await Task.FromResult(CreateUser(model));
		}
		#endregion
	}
}