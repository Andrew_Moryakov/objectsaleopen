﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using ObjectSale.Models;
using ObjectSale.Models.ViewModels;
using WebGrease.Extensions;

namespace ObjectSale.Controllers.Helpers.EntityHelpers
{
	public class VisitHelper
	{
		#region Async

		/// <summary>
		/// Асинхронно сохраняет в базе данных информацию о посещении, создает объект посещения
		/// </summary>
		/// <param name="visit">Объект посещения</param>
		/// <returns>Возвращает созданный объект</returns>
		public async Task<Visit> CreateVisitAsync(Visit visit)
		{
			return await Task.FromResult(CreateVisit(visit));
		}

		/// <summary>
		/// Асинхронно даляет посещение
		/// </summary>
		/// <param name="id">Идентификатор посещения, которое нужно удалить</param>
		/// <returns>Объект удаленного посещения</returns>
		public async Task<Visit> DeleteVisitAsync(int id)
		{
			return await Task.FromResult(DeleteVisit(id));
		}

		/// <summary>
		/// Асинхронно возвращает список всех посещений, с учётом фременных границ
		/// </summary>
		/// <param name="from">Начальная дата</param>
		/// <param name="to">Конечная дата</param>
		/// <returns>Возвращает список дат и количество посещений в каждой дате</returns>
		public async Task<VisitViewModel> GetFilteringVisitsOrderByMinutesAsync(string from = null, string to = null)
		{
			return await Task.FromResult(GetFilteringVisitsOrderByMinutes(from, to));
		}
		#endregion

		/// <summary>
		/// Возвращает список всех посещений, с учётом фременных границ
		/// </summary>
		/// <param name="from">Начальная дата</param>
		/// <param name="to">Конечная дата</param>
		/// <returns>Возвращает список дат и количество посещений в каждой дате</returns>
		public VisitViewModel GetFilteringVisitsOrderByMinutes(string from = null, string to = null)
		{
			List<Visit> filterVisits = GetFilteredDates(from, to);

			IEnumerable<IGrouping<string, string>> visitDate =
					filterVisits.Select(el => el.Date.Day + "." + el.Date.Month + "." + el.Date.Year).GroupBy(el => el).ToList();

			return new VisitViewModel(visitDate);
		}


		/// <summary>
		/// Сохраняет в базе данных информацию о посещении, создает объект посещения
		/// </summary>
		/// <param name="visit">Объект посещения</param>
		/// <returns>Возвращает созданный объект</returns>
		public Visit CreateVisit(Visit visit)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				ApplicationUser visitUser = db.Users.FirstOrDefault(el => el.UserName == visit.User.UserName);
				if (visitUser == null)
				{
					throw new Exception(Properties.Settings.Default.UserNotExist);
				}

				Visit visitToCreate = new Visit
				{
					Date = DateTime.Now,
					UserId = visit.UserId,
				};

				db.Visits.Add(visitToCreate);
				db.SaveChanges();

				return visitToCreate;
			}
		}

		/// <summary>
		/// Удаляет посещение
		/// </summary>
		/// <param name="id">Идентификатор посещения, которое нужно удалить</param>
		/// <returns>Объект удаленного посещения</returns>
		public Visit DeleteVisit(int id)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				Visit visit = db.Visits.Find(id);
				db.Visits.Remove(visit);
				db.SaveChanges();

				return visit;
			}
		}

		private List<Visit> GetFilteredDates(string from, string to)
		{
			using (ApplicationDbContext dbContext = new ApplicationDbContext())
			{
				List<Visit> visits = dbContext.Visits.Include(el=>el.User).ToList();

				if (from == null || to == null)
				{
					return visits;
				}

				DateTime fromDateTime = StringDateToObjDate(from).Date;
				DateTime toDateTime = StringDateToObjDate(to).Date;

				List<Visit> filterVisits = new List<Visit>();
				foreach (var visit in visits)
				{
					if (visit.Date.Date >= fromDateTime && visit.Date.Date <= toDateTime)
					{
						filterVisits.Add(visit);
					}
				}

				return filterVisits;
			}
		}

		/// <summary>
		/// Преобразует строкове представление даты в тип DateTime. В случае неудачного преобразование генерирует исключение
		/// </summary>
		/// <param name="date">Строка содержащая дату в строковм представлении</param>
		/// <returns>В случае успешного преобразования строки возвращает объект DateTime</returns>
		private DateTime StringDateToObjDate(string date)
		{
			DateTime dateTime;
			bool isValidFromDate = DateTime.TryParse(date, out dateTime);

			if (!(isValidFromDate))
			{
				throw new ArgumentException(Properties.Settings.Default.InvalidArguments, nameof(date));
			}

			return dateTime;
		}
	}
}