﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ObjectSale.Controllers.Helpers;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models;
using ObjectSale.Models.Attributes;
using ObjectSale.Models.ViewModels;

namespace ObjectSale.Controllers
{
	/// <summary>
	/// Класс для работы с заказами клиентов
	/// </summary>
	[Authorize]
	public class OrdersController: ApiController
	{
		/// <summary>
		/// Выполняет подтверждение заказа с указанным идентификатором
		/// </summary>
		/// <param name="id">Идентификатор заказа</param>
		/// <returns>Возвращает NoContent, если заказ успешно подтвержден</returns>
		[HttpPut]
		[AnyExceptionFilter]
		[ValidateModelState(typeof(OrdersController), "OrderExists")]
		public IHttpActionResult ConfirmOrder(int id)
		{
			OrderHelper helper = new OrderHelper();
			helper.ConfirmOrder(id);

			return StatusCode(HttpStatusCode.NoContent);
		}

		public List<OrderViewModel> GetOrders()
		{
			OrderHelper helper = new OrderHelper();
			return helper.GetOrders();
		}

		[Authorize(Roles = "employee")]
		[ResponseType(typeof(void))]
		[CheckModelForNull]
		[ValidateModelState(typeof(OrdersController), "OrderExists")]
		[AnyExceptionFilter]
		public IHttpActionResult PutOrder(Order order)
		{
				OrderHelper helper = new OrderHelper();
				helper.EditOrder(order);

				return StatusCode(HttpStatusCode.NoContent);
		}

		[ResponseType(typeof(Order))]
		[CheckModelForNull]
		[ValidateModelState]
		[AnyExceptionFilter]
		[CheckModelForNull]
		public IHttpActionResult PostOrder(List<Order> carts)
		{
			OrderHelper helper = new OrderHelper();
			helper.CreateOrder(carts);

			return StatusCode(HttpStatusCode.Created);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		[Authorize(Roles = "employee")]
		[ResponseType(typeof(Order))]
		[AnyExceptionFilter]
		[ValidateModelState(typeof(OrdersController), "OrderExists")]
		[CheckModelForNull]
		public IHttpActionResult DeleteOrder(int id)
		{
			OrderHelper helper = new OrderHelper();
			helper.DeleteOrder(id);

			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				return Ok(db.Orders.Find(id));
			}
		}

		protected override void Dispose(bool disposing)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				if (disposing)
				{
					db.Dispose();
				}
				base.Dispose(disposing);
			}
		}

		private bool OrderExists(Order order)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				return db.Orders.Find(order.Id)!=null;
			}
		}
	}
}