﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ObjectSale.Controllers.Helpers;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models;
using ObjectSale.Models.Attributes;
using ObjectSale.Models.ViewModels;

namespace ObjectSale.Controllers
{
	[Authorize]
	public class ProductsController: ApiController
	{
		[HttpGet]
		[AnyExceptionFilter]
		public List<ProductViewModel> GetProducts()
		{
			ProductHelper helper = new ProductHelper();

			return helper.GetProducts();
		}

		[Authorize(Roles = "employee")]
		[ResponseType(typeof(void))]
		[HttpPut]
		[CheckModelForNull]
		[ValidateModelState(typeof(ProductsController), "ProductExists")]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> EditProduct(Product model)
		{
			ProductHelper helper = new ProductHelper();
			await helper.EditProductAsync(model);

			return StatusCode(HttpStatusCode.NoContent);
		}

		[Authorize(Roles = "employee")]
		[ResponseType(typeof(Product))]
		[HttpPost]
		[CheckModelForNull]
		[ValidateModelState]
		[AnyExceptionFilter]
		public IHttpActionResult CreateProduct(Product model)
		{
			ProductHelper helper = new ProductHelper();
			model = helper.CreateProduct(model);

			return CreatedAtRoute("DefaultApi", new
			{
				Id = model.Id
			}, new
			{
				model.Title,
				model.SizesField,
				model.Price,
				model.Description,
				model.VendorCode,
				model.Id,
				model.CategoryId,
				model.Image
			});
		}

		[Authorize(Roles = "employee")]
		[ResponseType(typeof(Product))]
		[CheckModelForNull]
		[ValidateModelState(typeof(ProductsController), "ProductExists")]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> DeleteProduct(int id)
		{
			ProductHelper helper = new ProductHelper();
			helper.DeleteProduct(id);

			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				return Ok(DbContext.Products.Find(id)); //return Ok(product);
			}
		}

		protected override void Dispose(bool disposing)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				if (disposing)
				{
					DbContext.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		public static bool ProductExists(Product product)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				return DbContext.Products.Find(product.Id)!=null;
			}
		}

	}
}