﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNet.Identity;
using ObjectSale.Controllers.Helpers;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models;
using ObjectSale.Models.Attributes;
using ObjectSale.Models.ViewModels;

namespace ObjectSale.Controllers
{
	//[Authorize(Roles = "admin")]
	public class UserController: ApiController
	{
		[AnyExceptionFilter]
		public async Task<List<UserViewModel>> GetUsers()
		{
			UserHelper helper = new UserHelper();
			return helper.GetUsers();
		}

		[ResponseType(typeof(void))]
		[HttpPut]
		[CheckModelForNull]
		[ValidateModelState(typeof(UserController), "ApplicationUserExists")]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> UpdateUser(RegisterBindingModel model)
		{
			UserHelper helper = new UserHelper();
			await helper.EditUserAsync(model);

			return StatusCode(HttpStatusCode.NoContent);
		}

		[ResponseType(typeof(ApplicationUser))]
		[HttpPost]
		[CheckModelForNull]
		[ValidateModelState]
		[AnyExceptionFilter]
		public async Task<IHttpActionResult> CreateUser(RegisterBindingModel model)
		{
			UserHelper helper = new UserHelper();

			IdentityResult result;

			result = await helper.CreateUserAsync(model);

			if (result.Succeeded)
			{
				return Ok();
			}

			return CreatedAtRoute("userRoute", new
			{
				id = model.Id
			}, model);
		}

		[ResponseType(typeof(ApplicationUser))]
		[HttpDelete]
		[AnyExceptionFilter]
		[ValidateModelState(typeof(UserController), "ApplicationUserExists")]
		[CheckModelForNull]
		public async Task<IHttpActionResult> DeleteUser(string id)
		{
				UserHelper helper = new UserHelper();
				ApplicationUser user = await helper.DeleteUserAsync(id);

				return Ok(user);
		}

		protected override void Dispose(bool disposing)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				if (disposing)
				{
					DbContext.Dispose();
				}
			}

			base.Dispose(disposing);
		}

		private bool ApplicationUserExists(ApplicationUser user)
		{
			using (ApplicationDbContext DbContext = new ApplicationDbContext())
			{
				return DbContext.Users.Find(user.Id) != null;
			}
		}

		private IHttpActionResult GetErrorResult(IdentityResult result)
		{
			if (result == null)
			{
				return InternalServerError();
			}

			if (!result.Succeeded)
			{
				if (result.Errors != null)
				{
					foreach (string error in result.Errors)
					{
						ModelState.AddModelError("", error);
					}
				}

				if (ModelState.IsValid)
				{
					// No ModelState errors are available to send, so just return an empty BadRequest.
					return BadRequest();
				}

				return BadRequest(ModelState);
			}

			return null;
		}
	}
}