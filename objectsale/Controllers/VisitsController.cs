﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using ObjectSale.Controllers.Helpers;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models;
using ObjectSale.Models.Attributes;

namespace ObjectSale.Controllers
{
	public class VisitsController: ApiController
	{
		[AnyExceptionFilter]
		public dynamic GetVisits()
		{
			VisitHelper helper = new VisitHelper();
			return helper.GetFilteringVisitsOrderByMinutes();
		}

		[AnyExceptionFilter]
		public dynamic GetVisits(string from, string to)
		{
			VisitHelper helper = new VisitHelper();
			return helper.GetFilteringVisitsOrderByMinutes(from, to);
		}

		[ResponseType(typeof(Visit))]
		[CheckModelForNull]
		[AnyExceptionFilter]
		[ValidateModelState(typeof(VisitsController), "ApplicationUserExists")]//Проверяем существует ли пользователь который посетил
		public async Task<IHttpActionResult> PostVisit(Visit visit)
		{
			
			VisitHelper visitHelper = new VisitHelper();
			visit = await visitHelper.CreateVisitAsync(visit);

			return CreatedAtRoute("DefaultApi", new
			{
				id = visit.Id
			}, visit);
		}

		[ResponseType(typeof(Visit))]
		[AnyExceptionFilter]
		[CheckModelForNull]
		[ValidateModelState(typeof(VisitsController), "VisitExists")]
		public async Task<IHttpActionResult> DeleteVisit(int id)
		{
			VisitHelper visitHelper = new VisitHelper();
			Visit visit = await visitHelper.DeleteVisitAsync(id);

			return Ok(visit);
		}

		protected override void Dispose(bool disposing)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				if (disposing)
				{
					db.Dispose();
				}
				base.Dispose(disposing);
			}
		}

		/// <summary>
		/// Проверяет данные посещения на предмет существования пользователя
		/// </summary>
		/// <param name="visit"></param>
		/// <returns></returns>
		public static bool ApplicationUserExists(Visit visit)
		{
			if (visit.User==null)
			{
				return false;
			}

			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				return db.Users.Count()>0 && db.Users.Any(el => el.UserName == visit.User.UserName);
			}
		}

		public static bool VisitExists(Visit visit)
		{
			using (ApplicationDbContext db = new ApplicationDbContext())
			{
				return db.Visits.Find(visit.Id) != null;
			}
		}
	}
}