namespace ObjectSale.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationsName : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Carts", "Confirmed", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Carts", "Confirmed");
        }
    }
}
