﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using ObjectSale.Controllers.Helpers.EntityHelpers;
using ObjectSale.Models.ViewModels;

namespace ObjectSale.Models
{
	public class AppDbInitializer : DropCreateDatabaseAlways<ApplicationDbContext>
	{
		protected override void Seed(ApplicationDbContext context)
		{
			//UserHelper helper = new UserHelper();
			string emailForAdmin = "admin@ObjectSale.org";
			string admin = "admin";

			//UserViewModel adminInDb = helper.GetUsers().FirstOrDefault(user => user.Email == emailForAdmin);

			//if (adminInDb == null)
			//{
			var userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(context));
				var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

				var role = new IdentityRole
				{
					Name = "admin"
				};
			var roleEmployee = new IdentityRole
			{
				Name = "employee"
			};
			var roleUser = new IdentityRole
			{
				Name = "user"
			};

			roleManager.Create(role);
			roleManager.Create(roleEmployee);
			roleManager.Create(roleUser);

			var adminUser = new ApplicationUser
				{
					Email = emailForAdmin,
					UserName = emailForAdmin,
					Adress = admin,
					SurName = admin,
					Name = admin
				};
				const string password = "123456";
				var result = userManager.Create(adminUser, password);

				if (result.Succeeded)
				{
					userManager.AddToRole(adminUser.Id, role.Name);
				}

				base.Seed(context);

			//}
		}
	}
}
