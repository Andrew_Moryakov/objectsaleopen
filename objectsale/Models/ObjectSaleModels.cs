﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ObjectSale.Models
{
	[NotMapped]
	public class UploadFile
	{
		public string Name{get; set;}
		public string File{get; set;}
		public string Do{get; set;}
	}

	public class Product
	{
		public int Id{get; set;}
		[Required]
		public String Title{get; set;}

		[Required]
		[Range(typeof(decimal), "0", "1000000")]
		public decimal Price {get; set;}

		[Required]
		[Range(typeof(int), "0", "2147483647")]
		public int VendorCode{get; set;}

		[NotMapped]
		public UploadFile Image{get; set;}

		public string ImagePath{get; set;}
		public string Description{get; set;}

		public int? CategoryId{get; set;}
		[ForeignKey("CategoryId")]
		public Category Category{get; set;}

		[NotMapped]
		public int[] SizesField{get; set;}

		public ICollection<Size> Sizes{get; set;}

		public Product()
		{
			Sizes = new List<Size>();
		}
	}

	public class Size
	{
		[Key]
		public int Id{get; set;}

		[Required(ErrorMessage = "Поле должно быть установлено")]
		[Range(typeof(int), "0", "2147483647")]
		public int SizeСode{get; set;}

		[Required]
		public int ProductId{get; set;}
		[ForeignKey("ProductId")]
		public Product Product{get; set;}
	}

	public class Category
	{
		public int Id{get; set;}

		[Required(ErrorMessage = "Должен быть введен заголовок категории")]
		public String Title{get; set;}
		public String Description{get; set;}
	}

	public class Order
	{
		[Key]
		public int Id{get; set;}

		[Required]
		public int ProductId { get; set; }
		[ForeignKey("ProductId")]
		public Product Product { get; set; }

		[Required(ErrorMessage = "Должно быть определено количество товара")]
		[Range(typeof(int), "0", "2147483647")]
		public int CartCount{get; set;}

		[Required]
		[Range(typeof(int), "0", "2147483647")]
		public int SelectedSize { get; set; }

		public bool Confirmed{ get; set; }

		public String UserId { get; set; }
		[NotMapped]
		public ApplicationUser User{get; set;}
	}

	public class Visit
	{
		public int Id{get; set;}
		public DateTime Date{get; set;}

		public String UserId{get; set;}
		[ForeignKey("UserId")]
		public ApplicationUser User{get; set;}
	}
}
