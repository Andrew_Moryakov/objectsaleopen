﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectSale.Models.ViewModels
{
	public class OrderViewModel
	{
		public OrderViewModel(Order order, List<ApplicationUser> users)
		{
			ProductTitle = order.Product.Title;
			SelectedSize = order.SelectedSize;
			CartCount = order.CartCount;
			Email = users.FirstOrDefault(el => el.Id == order.UserId)?.UserName;
			Id = order.Id;
		}

		public string ProductTitle{ get; }
		public int SelectedSize { get; }
		public int CartCount { get; }
		public string Email { get; }
		public int Id { get; }
	}
}
