﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectSale.Models.ViewModels
{
	public class ProductViewModel
	{
		public ProductViewModel(Product product)
		{
			Id = product.Id;
			Title = product.Title;
			Price = product.Price;
			Description = product.Description;
			VendorCode = product.VendorCode;
			CartCount = 0;
			CategoryId = product.CategoryId;
			Image = new
			{
				Name = product.ImagePath
			};
			SizesField = product.Sizes?.Select(size => size.SizeСode).ToArray();
		}

		public int Id { get; }
		public int[] SizesField { get; }
		public decimal Price { get; }
		public string Description { get; }
		public int VendorCode { get; }
		public int CartCount { get; }
		public string Title { get; }
		public dynamic Image { get; }
		public int? CategoryId { get; }
	}
}
