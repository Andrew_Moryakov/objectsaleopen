﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using ObjectSale.Properties;

namespace ObjectSale.Models.ViewModels
{
	public class UserViewModel
	{
		public UserViewModel(ApplicationUser user, List<IdentityRole> roles )
		{
			Id = user.Id;
			Name = user.Name;
			SurName = user.SurName;
			Adress = user.Adress;
			Email = user.Email;
			Role = roles.FirstOrDefault(role =>
										{
											var identityUserRole = user.Roles.FirstOrDefault();
											return identityUserRole != null && role.Id == identityUserRole.RoleId;
										}).Name;

			Roles = new
			{
				Employee = Settings.Default.RoleEmployee,
				User = Settings.Default.RoleUser
			};
		}

		public string Id{ get; }
		public string Name{ get; }
		public string SurName { get; }
		public string Adress { get; }
		public string Email { get; }
		public string Role { get; }
		public dynamic Roles{ get; }
	}
}
