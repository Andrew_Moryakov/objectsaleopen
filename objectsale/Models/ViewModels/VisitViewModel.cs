﻿using System.Collections.Generic;
using System.Linq;

namespace ObjectSale.Models.ViewModels
{
	public class VisitViewModel
	{
		private readonly string[] _visitDates;
		private readonly int[] _visitCounts;

		public VisitViewModel(IEnumerable<IGrouping<string, string>> groupingVisits)
		{
			_visitDates = groupingVisits.Select(el => el.Key).ToArray();
			_visitCounts = groupingVisits.Select(el => el.Count()).ToArray();
		}

		public string[] VisitDates => _visitDates;
		public int[] VisitCounts => _visitCounts;
	}
}
