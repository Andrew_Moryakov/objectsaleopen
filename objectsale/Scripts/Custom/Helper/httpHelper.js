﻿//Произвоит перенаправление на указанную страницу - методом POST
function post(path, params, method) {
	method = method || "post";
	var form = document.createElement("form");
	form.setAttribute("method", method);
	form.setAttribute("action", path);

	for (var key in params) {
		if (params.hasOwnProperty(key)) {
			var hiddenField = document.createElement("input");
			hiddenField.setAttribute("type", "hidden");
			hiddenField.setAttribute("name", key);
			hiddenField.setAttribute("value", params[key]);

			form.appendChild(hiddenField);
		}
	}

	document.body.appendChild(form);
	form.submit();
}

var jqData;
///Выполняет Http запрос
function httpRequest(url, isAsync, dataToServer, type, isAuthorization) {
	$.ajax({
		type: type,
		async: isAsync,
		url: url,
		dataType: "json",
		data: dataToServer,
		success: function (data) {
			jqData = data;
		},
		beforeSend: function (xhr) {
			if (isAuthorization) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		}
	});
	return jqData;
}

///Выполняет Http Get Ajax запрос. Возвращает полученные данные, в случае успеха
function HttpGetJqAjax(url, isAsync, dataToServer, isAuthorization) {
	if (typeof isAuthorization == "undefined") {
		isAuthorization = true;
	}
	return httpRequest(url, isAsync, dataToServer, "GET", isAuthorization);
}

///Выполняет Http Post Ajax запрос. Возвращает полученные данные, в случае успеха
function HttpPostJqAjax(url, isAsync, dataToServer, isAuthorization) {
	if (typeof isAuthorization == "undefined") {
		isAuthorization = true;
	}
	return httpRequest(url, isAsync, dataToServer, 'POST', isAuthorization);
}