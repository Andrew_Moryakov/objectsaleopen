﻿///Ключ, для получения доступа к токену из локального хранилища
var tokenKey = "ObjectSaleLogIn";
var linkToAdmin = "/Admin/Index";
var linkToEmployee = "/Employee/Index";
var linkToStore = "/Store/Index";

///Возвращает объект - токен
function getTokenObjFromLocalStorage() {
	var tokenString = localStorage.getItem(tokenKey);
	var tokenObject = JSON.parse(tokenString);
	return tokenObject;
}

///Возвращает строку, которая содержит токен доступа
function getAccessTokenFromLocalStorage() {
	var tokenObject = getTokenObjFromLocalStorage();

	if (tokenObject === null) {
		return null;
	}

	return tokenObject.access_token;
}

///Устанавливает значение в локальное хранилище с ключем для хранения токена доатсупа
function setTokenToLocalStorage(token) {
	localStorage.setItem(tokenKey, JSON.stringify(token));
}

///Удаляет из локального хранилища токен доступа
function logOut() {
	localStorage.removeItem(tokenKey);
}

///Выполняет вход. Возвращает токен доступа
function logIn(email, password) {
	var loginData = {
		grant_type: 'password',
		username: email,
		password: password
	};

	return HttpPostJqAjax('/Token', false, loginData, false);
}

///Возвращает ссылку в личный кабинет пользователя, в зависимости от роли
function GetPrivateOfficeLink() {
	var tokenObj = getTokenObjFromLocalStorage();

	if (tokenObj != null) {
		if (tokenObj.role === "employee") {
			return linkToEmployee;
		} else if (tokenObj.role === "admin") {
			return linkToAdmin;
		} else {
			return linkToStore;
		}
	}

	return '';
};

///Возвращает имя пользователя
function getUserNameFromToken() {
	var tokenObj = getTokenObjFromLocalStorage();
	if (tokenObj != null) {
		return tokenObj.userName;
	} else {
		return null;
	}
};