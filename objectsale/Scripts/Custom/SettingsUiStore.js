﻿var settingUiStore = {
	buttons: {
		buyButton: {
			title: "Купить"
		},
		toCartButton: {
			title: "В корзину"
		},
		ascendingButton: {
			title: "Отобразить в начале самые низкие цены"
		},
		descendingButton: {
			title: "Отобразить в начале самые высокие цены"
		},
		makeOrder: {
			title:"Заказать"
		}
	},
	headers: {
		sortButtonsHeader: {
			title: "Сортируйте по цене"
		},
		categoryListHeader: {
			title: "Выберете категорию"
		},
		categoryAllOrdersHeader: {
			title: "Все товары"
		},
		productDescriptionHeader: {
			title: "Описание: "
		},
		productPriceHeader: {
			title: "Цена: "
		},
		productSizesHeader: {
			title: "Выберите размер:"
		},
		productSizeNotSelectedHeader: {
			title: "Пожалуйста, выберите размер, прежде чем купить."
		},
		fullCost: {
			title: "Общая стоимость "
		},
		size: {
			title:"Размер:"
		}
	},
	topMenu: {
		linkStore: {
			title:"Магазин"
		},
		linkLogOut: {
			title:"Выйти"
		},
		linkCart: {
			title: "Корзина"
		}
	},

	messages: {
		currencySymbol: {
			title: " ₽"
		},
		orderIsAccepted: {
			title: "Ваш заказ принят. Спасибо!"
		},
		cartIsEmpty: {
			title: "Корзина пуста"
		}
	}
}