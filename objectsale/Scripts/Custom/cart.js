﻿var appCart = angular.module("appCart", []);

var getSelectedSizeOfProduct = function(product)
{
	return Enumerable.From(product.SizesField).Where(function (size) {
		return  size.selected == true;
	}).ToArray()[0].size;
}

appCart.controller("Cart", function($scope, $http) {
	$scope.successfully = false;
	$scope.settingUiStore = settingUiStore;

	$scope.initCart = function() {
		$scope.products = JSON.parse(localStorage.getItem(productsKey));
		$scope.totalCartCount = $scope.totalCount();

		$scope.PrivateOfficeLink = GetPrivateOfficeLink();
		$scope.userName = getUserNameFromToken();
	};

	$scope.getSelectedSize = function(product) {
		return getSelectedSizeOfProduct(product);
	}

	$scope.addToCart = function(product) {
		product.CartCount++;
		$scope.totalCartCount++;
	};

	$scope.removeFromCart = function(product) {
		if (product.CartCount > 0) {
			product.CartCount--;
			$scope.totalCartCount--;
		}
	};

	$scope.makeCart = function() {
		$(".ui.page.dimmer").dimmer("show");
		localStorage.setItem(productsKey, "[]");
		$scope.successfully = true;

		var token = getTokenObjFromLocalStorage();
		var orders = Enumerable.From($scope.products).Where(function(el) { return el.CartCount > 0 });

		var dataPost = [];
		orders.ForEach(function(el) {
			dataPost.push({
				"ProductId": el.Id.toString(),
				"User": { "UserName": token.userName },
				"CartCount": el.CartCount.toString(),
				"SelectedSize": getSelectedSizeOfProduct(el)

			});
		});

		$http.post(cartUrl, dataPost, { headers: { "Authorization": "Bearer " + token.access_token } })
			.success(function(data) {
				$scope.totalCartCount = 0;
				$(".ui.page.dimmer").dimmer("hide");
			}).error(function(response) {

			});
	};

	$scope.totalCount = function() {
		var r = Enumerable.From($scope.products).Select(function(el) { return el.CartCount }).Sum();
		return r;
	};
	$scope.totalCost = function() {
		var r = Enumerable.From($scope.products).Select(function(el) { return el.CartCount * el.Price }).Sum();
		return r;
	};
});