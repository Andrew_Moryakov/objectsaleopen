﻿function addItemCategory() {
	$("#jsGridTritium").jsGrid("insertItem", {
		Title: $("#titleCategory").val(),
		Description: $("#descriptionCategory").val()
	});
	$(".ui.basic.modal").modal("hide");
	//    .done(function () {
	//    console.log("insertion completed");
	//});
}

var currentItem;
var clickRoleItem;
$("#gridTab").on("click", ".roleinput", function(val) {
	$(".roleinput").removeAttr("checked");
	$(".roleinput").parent(".slider").attr("class", "ui slider checkbox");
	$(".roleinput").prop("checked", false);

	$(this).prop("checked", true);
	$(this).attr("checked", "checked");
	$(this).parent(".slider").attr("class", "ui slider checkbox checked");
	clickRoleItem = $(this).attr("role");
});
 

var uriCategories = "/api/Categories/";
var dbCategoriesssss = {
	loadData: function() {
		return $.ajax({
			type: "GET",
			url: uriCategories,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	},
	insertItem: function(item) {
		return $.ajax({
			type: "POST",
			url: uriCategories,
			data: item,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	},

	updateItem: function(item) {
		return $.ajax({
			type: "PUT",
			url: uriCategories,
			data: item,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	},

	deleteItem: function(item) {
		return $.ajax({
			type: "DELETE",
			url: uriCategories + item.Id,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	}
};

$(function() {

	$("#jsGridTritium").jsGrid({
		height: "100%",
		width: "100%",
		filtering: false,
		inserting: false,
		editing: true,
		sorting: true,
		paging: true,
		autoload: true,
		loadIndication: true,

		pageSize: 10,
		pageButtonCount: 5,
		deleteConfirm: "Вы действительно хотите удалить?",

		controller: dbCategoriesssss,
		fields: [
			{ name: "Title", type: "text", title: "Название" },
			{ name: "Description", width: 100, type: "text", title: "Описание" },
			{ type: "control" }
		]
	});

});