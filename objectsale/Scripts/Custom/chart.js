﻿$('body').on('click', '#chartTab', function (val) {
	chartGo(null);
});

var statisticSelectedPeriod;
function chartGo(data) {
	statisticSelectedPeriod = downloadData(data);
	setParams(statisticSelectedPeriod.VisitCounts, statisticSelectedPeriod.VisitDates);
	drawChart();
	winResize();

	getTodayVisits();
	getMonthVisits();
	getSelectedPeriodVisits();
}

function getToDayJson() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth() + 1; //January is 0!
	var yyyy = today.getFullYear();

	return {
		dd: dd,
		mm: mm,
		yyyy:yyyy
	}
}

function downloadStatistic(from, to) {
	var filter = { "from": from, "to": to };
	return HttpGetJqAjax(visitUrl, false, filter);
}

function statisticToHeader(header, statistic) {
	var visitsToDay = Enumerable.From(statistic.VisitCounts).Select(function (el) { return el; }).Sum();
	header.empty();
	header.append(visitsToDay);
}

function getTodayVisits() {
	var today = getToDayJson();

	var stringToday = today.dd + "." + today.mm + "." + today.yyyy;

	var todayStatistic = downloadStatistic(stringToday, stringToday);

	statisticToHeader($("#statisticToday"), todayStatistic);
}

function getMonthVisits() {
	var today = getToDayJson();

	var stringStartMonth = 1 + "." + today.mm + "." + today.yyyy;
	var stringEndMonth = today.dd + "." + today.mm + "." + today.yyyy;

	var monthStatistic = downloadStatistic(stringStartMonth, stringEndMonth);

	statisticToHeader($("#statisticMonth"), monthStatistic);
}

function getSelectedPeriodVisits() {
	statisticToHeader($("#statisticSelectedPeriod"), statisticSelectedPeriod);
}

function downloadData(filter) {

	return HttpGetJqAjax(visitUrl, false, filter);


}


var dataForChart = {};
var currentChart;


function winResize() { 
	$(window).resize(function () {
		if (currentChart) {
			setChartDimensions();
			currentChart.stop();
			currentChart.resize(currentChart.render, true);
		}
	});
}

function setChartDimensions() {
	var width = $("section").width(),
		height = $("footer").offset().top - $('canvas').offset().top - 20;
	if (currentChart) {
		currentChart.chart.aspectRatio = width / height;
	} else {
		$('canvas').attr("width", width);
		$('canvas').attr("height", height);
	}
}

function setParams(visitCounts, visitDates) {
	dataForChart = {
		labels: visitDates,
		datasets: [
			{
				label: "Статистика посещений",
				fillColor: "rgba(220,0,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: visitCounts
			}
		]
	};
}

function resetCanvas () {
	$('canvas').remove();
	var newCanvas = $('<canvas><canvas>');
	$('#chartContainer').append(newCanvas);

	return newCanvas;
};

function drawChart() { 
	setChartDimensions();
	var ctx = $("canvas")[0].getContext("2d");

	if (currentChart != null) {
		currentChart.destroy();
	}

	var myoptions = {
		scaleShowGridLines: true,
		responsive: true,
		animation: true,

		tooltipTemplate: "<%= value + ' посещений' %>",
		multiTooltipTemplate: "<%= value + ' посещений' %>",
	}

	currentChart = new Chart(ctx).Line(dataForChart, myoptions);
}