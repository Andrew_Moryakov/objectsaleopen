﻿$(function () {
	$.datepicker.setDefaults($.datepicker.regional['ru']);

	var dateFrom;
	var dateTo;
	$('#datepickerFrom, #datepickerTo').datepicker({
		onSelect: function(date, datepicker) {
			if (datepicker.id == "datepickerFrom") {
				dateFrom = new Date( datepicker.currentYear, datepicker.currentMonth, datepicker.selectedDay);
			}
			if (datepicker.id == "datepickerTo") {
				dateTo = new Date(datepicker.currentYear, datepicker.currentMonth, datepicker.selectedDay);
			}
			if (dateTo >= dateFrom) {
				$("#btnFilter").show();
			} else {
				$("#btnFilter").hide();
			}
		}
	});
});

$('body').on('click', '#btnFilter', function (val) {
	$(".ui.page.dimmer").dimmer("show");
	chartGo({ "from": $("#datepickerFrom").val(), "to": $("#datepickerTo").val() });
	$(".ui.page.dimmer").dimmer("hide");
});
