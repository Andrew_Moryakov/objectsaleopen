﻿//jQuery(document).ready(function ($) {
//    tab = $('.tabs h3 a'); 
//    tab.on('click', function (event) {
//        event.preventDefault();
//        tab.removeClass('active');
//        $(this).addClass('active');

//        tab_content = $(this).attr('href');
//        $('div[id$="tab-content"]').removeClass('active');
//        $(tab_content).addClass('active');
//    });
//});


$(function () {
    $('#submit').click(function (e) {
        e.preventDefault();
        if (!formreg.valid()) {
            return;
        }
        
         
        var data = {
            Name: $('#name').val(),
            SurName: $('#surName').val(),
            Adress: $('#adress').val(),
            Email: $('#email').val(),
            Role: $("#userRole").prop("checked") ? "user" : "employee",
            Password: $('#password').val(),
            ConfirmPassword: $('#passwordConfirm').val()
        };

        $.ajax({
            type: 'POST',
            url: '/api/user',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data)
        }).success(function (data) {
            alert("Регистрация пройдена");
        }).fail(function (data) {
            alert("В процесе регистрации возникла ошибка");
        });
    });
})