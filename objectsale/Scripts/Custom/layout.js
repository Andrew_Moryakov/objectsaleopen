﻿var layoutApp = angular.module("layoutApp", []);

layoutApp.controller("layout", function ($scope, $http) {

	$scope.settingUiStore = settingUiStore;

	$scope.initLayout = function () {
		$scope.userName = getUserNameFromToken();
		$scope.PrivateOfficeLink = GetPrivateOfficeLink();
	};

});