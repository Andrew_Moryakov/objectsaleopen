﻿$(document).ready(function() {
	$('#submit').click(function(e) {
		e.preventDefault();

		$(".ui.page.dimmer").dimmer("show");

		var token = logIn($('#email').val(), $('#password').val());

		if (typeof token == "undefined") {
			$(".ui.page.dimmer").dimmer("hide");
			alert('При логине возникла ошибка');
		} else {
			setTokenToLocalStorage(token);
			var urlForRedirect = GetPrivateOfficeLink();
			post(urlForRedirect, "[]");
		}

	});
});