﻿var productModal = {
    modalTitle: "Создайте новый продукт",
    modalDescription:"Заполните все поля",
    one: {
        id: "titleProduct",
        title: "Название",
        display: "block"
    },
    two: {
        id: "imageModalCard",
        title: "Изображение",
        display: "block"
    },
    three: {
        id: "sizeProduct",
        title: "Размер",
        display: "block"
    },
    four: {
        id: "priceProduct",
        title: "Цена",
        display: "block"
    },
    five: {
        id: "descriptionProduct",
        title: "Описание",
        display: "block"
    },
    six: {
        id: "vendorCodeProduct",
        title: "Артикул",
        display: "block"
    },
    seven: {
        id: "modalCategory",
        title: "Категория",
        display: "block"
    }
};

var categoryModal = {
    modalTitle: "Создайте новую категорию",
    modalDescription: "Заполните все поля",
    one: {
        id: "titleCategory",
        title: "Название",
        display: "block"
    },
    two: {
        display: "none"
    },
    three: {
        display: "none"
    },
    four: {
        display: "none"
    },
    five: {
        id: "descriptionCategory",
        display: "block"
    },
      six: {
        display: "none"
      },
      seven: {
          display: "none"
      }
};

//Всё, что ниже нужно будет заменить на AngularJs. Получится гораздо меньше сток кода
$("#createModal").click(function () { //Настраиваем модальное окно для создания продукта

    $("#modalTitle").text(productModal.modalTitle);
    $("#modalDescription").text(productModal.modalDescription);

    if (typeof $("#titleProduct") == "undefinde") {//Если до этого создавали категорию и изменили Id поля для воода имени, то меняем обратно
        $("#titleCategory").attr("id", productModal.one.id);
          }
    $("#" + productModal.one.id) .attr("id", productModal.one.id);
    $("#"+productModal.one.id)   .  attr("placeholder", productModal.one.text);
    $("#" + productModal.one.id) .attr("style", "display:" + productModal.one.display);

    $("#" + productModal.two.id).attr("id", productModal.two.id);
  //  $("#" + productModal.two.id).attr("placeholder", productModal.two.text);
    $("#" + productModal.two.id).attr("style", "display:" + productModal.two.display);

    $("#" + productModal.three.id).attr("id", productModal.three.id);
   // $("#" + productModal.three.id).attr("placeholder", productModal.three.text);
    $("#" + productModal.three.id).attr("style", "display:" + productModal.three.display);

    $("#" + productModal.four.id).attr("id", productModal.four.id);
    $("#" + productModal.four.id).attr("placeholder", productModal.four.text);
    $("#" + productModal.four.id).attr("style", "display:" + productModal.four.display);

    if (typeof $("#descriptionProduct") == "undefinde") {
        $("#descriptionCategory").attr("id", productModal.five.id);
    }
    $("#" + productModal.five.id).attr("id", productModal.five.id);
    $("#" + productModal.five.id).attr("placeholder", productModal.five.text);
    $("#" + productModal.five.id).attr("style", "display:" + productModal.five.display);

    $("#" + productModal.six.id).attr("id", productModal.six.id);
    $("#" + productModal.six.id).attr("placeholder", productModal.six.text);
    $("#" + productModal.six.id).attr("style", "display:" + productModal.six.display);

    $("#" + productModal.seven.id).attr("id", productModal.seven.id);
    $("#" + productModal.seven.id).attr("placeholder", productModal.seven.text);
    $("#" + productModal.seven.id).attr("style", "display:" + productModal.seven.display);
    
    var r = $("#addProduct").attr("id");
    if (r == "undefined") {
        $("#addCategory").attr("id", "addProduct");
    }

    $('.ui.basic.modal').modal('show');
});

$("#createModalCategory").click(function () {
    
    $("#modalTitle").text(categoryModal.modalTitle);
    $("#modalDescription").text(categoryModal.modalDescription);

    if (typeof $("#titleProduct") != "undefinde") {
        $("#titleProduct").attr("id", "titleCategory");
    }

    $("#" + productModal.one.id).attr("id", categoryModal.one.id);
    $("#" + productModal.one.id).attr("placeholder", categoryModal.one.text);
    $("#" + productModal.one.id).attr("style", "display:" + categoryModal.one.display);

    if (typeof $("#descriptionProduct") != "undefinde") {
        $("#descriptionProduct").attr("id", "descriptionCategory");
    }
    $("#descriptionCategory").attr("id", categoryModal.five.id);
    $("#descriptionCategory").attr("placeholder", categoryModal.five.text);
    $("#descriptionCategory").attr("style", "display:" + categoryModal.five.display);

    $("#" + productModal.two.id).attr("style", "display:" + categoryModal.two.display);
    $("#" + productModal.three.id).attr("style", "display:" + categoryModal.three.display);
    $("#" + productModal.four.id).attr("style", "display:" + categoryModal.four.display);
    $("#" + productModal.six.id).attr("style", "display:" + categoryModal.six.display);
    $("#" + productModal.seven.id).attr("style", "display:" + categoryModal.seven.display);

    var r = $("#addCategory").attr("id");
    if (typeof  r == "undefined") {
        $("#addProduct").attr("id", "addCategory");
    }
    
    $('.ui.basic.modal').modal('show');
});

$('body').on('click', '#addCategory', function (val) {
    addItemCategory();//categoryGrid.js
});
$('body').on('click', '#addProduct', function (val) {
    addItemProduct();//productsGrid.js
});

//$("#addCategory").click(function () {
 
//});
//$("#addProduct").click(function () {
    
//});