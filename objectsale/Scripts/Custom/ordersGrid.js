﻿$('body').on('click', '.ui.primary.button', function(val) {
	$.ajax({
		type: "PUT",
		url: uri + 'ConfirmOrder/' + $(this).attr('item'),
		data: $(this).attr('item'),
		dataType: "json",
		beforeSend: function(xhr) {
			xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
		}
	});

	$("#jsGridorders").jsGrid("loadData");
	$("#jsGridorders").jsGrid("render");
});

var uri = "/api/Orders/";
var DbContextCategories = {
	loadData: function(data) {
		return $.ajax({
			type: "GET",
			url: uri,
			data: data,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	},

	insertItem: function(item) {
		return $.ajax({
			type: "POST",
			url: uri,
			data: item,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	},

	updateItem: function(item) {
		return $.ajax({
			type: "PUT",
			url: uri,
			data: item,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	},

	deleteItem: function(item) {
		return $.ajax({
			type: "DELETE",
			url: uri + item.Id,
			dataType: "json",
			beforeSend: function(xhr) {
				xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
			}
		});
	}
};

var okField = function (config) {
	jsGrid.Field.call(this, config);
};

okField.prototype = new jsGrid.Field({
	itemTemplate: function (value, item) {
		return $('<div item="'+item.Id+'" id="button'+item.Id+'" class="ui primary button">Подтвердить заказ</div>');

	}
});

$(function () {

	jsGrid.fields.ok = okField;

	$("#jsGridorders").jsGrid({

		height: "100%",
		width: "100%",
		filtering: false,
		inserting: false,
		editing: true,
		sorting: true,
		paging: true,
		autoload: true,
		loadIndication: true,
		editRowRenderer: null,


		pageSize: 10,
		pageButtonCount: 5,
		deleteConfirm: "Вы действительно хотите удалить?",

		controller: DbContextCategories,
		fields: [
			{ name: "ProductTitle", type: "text", title: "Продукт", editing: false },
			{ name: "SelectedSize", type: "text", title: "Размер" },
			{ name: "CartCount", type: "text", title: "Количество" },
			{ name: "Email", width: 100, type: "text", title: "Клиент", editing: false },
			{ width: 100, type: "ok", title: "Подтвердить", editing: false },
			{ type: "control" }
		]
	});

});