﻿function addItemProduct() {
    var imageCreate = document.getElementById('hideFileProduct').files[0];

        var dataBase64;
        fileToBase64(imageCreate, function(e) {
            dataBase64 = {
                File: btoa(e.target.result),
                Name: imageCreate.name,
                Do: "create"
            };

            $("#jsGrid").jsGrid("insertItem", {
                Title: $("#" + productModal.one.id).val(),
                Image: dataBase64,
                SizesField: $('#dropDownSizeProduct').attr("value").split(","),
                Price: $("#" + productModal.four.id).val(),
                Description: $("#" + productModal.five.id).val(),
                VendorCode: $("#" + productModal.six.id).val(),
                CategoryId: $("#categoryProduct option:selected").val()
            });
        
            $('.ui.basic.modal').modal('hide');
        });

}

$('body').on('click', '#imgPreviewClose', function (val) {  
    $("#imgPreview").attr("src", "");
    $("body").find("#imgPreviewСontainer").attr("display", "none");
    isImageDelete = true;
});

var isImageDelete = false;
var base64Data;
var base64line;
var setOfSizes = [];
var currentDD;
var resultCurrentSizes = [];
var file= undefined;
var uriProduct = '/api/Products/';


var dbProducts = {
    loadData: function () {
        return $.ajax({
            type: "GET",
            url: uriProduct,
            dataType: "json",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
            }
        });
    },

    insertItem: function (item) { 
        return $.ajax({
            type: "POST",
            url: uriProduct,
            data: item,
            dataType: "json",
            beforeSend: function (xhr) {
                var tokenObj = JSON.parse(localStorage.getItem(tokenKey));
                xhr.setRequestHeader("Authorization", "Bearer " + tokenObj.access_token);
            }
        });
    },

    updateItem: function (item) {
        if (typeof file != "undefined") {
            fileToBase64(file, function(e) {
                if (file != null) {
                    item.Image = {
                        File: btoa(e.target.result),
                        Name: file.name,
                        Do: "update"
                    };
                } else {
                    item.File = null;
                }

                file = window.undefind;
                return $.ajax({
                    type: "PUT",
                    url: uriProduct,
                    data: item,
                    dataType: "json",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
                        },
                    success: function() {
                        item.Image.Name = null;
                    }
                });
            });
        } else {
            item.File = null;
            if (isImageDelete) {
                item.Image.Do = "delete";
            }
            return $.ajax({ 
                type: "PUT",
                url: uriProduct,
                data: item,
                dataType: "json",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
                    }
            });
        }
    },

    deleteItem: function (item) {
        return $.ajax({
            type: "DELETE",
            url: uriProduct + item.Id,
            dataType: "json",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
                }
        });
    }
};

var RoleField = function (config) {
    jsGrid.Field.call(this, config);
};
var multipleSelectionsField = function (config) {
    jsGrid.Field.call(this, config);
};
var fileField = function (config) {
    jsGrid.Field.call(this, config);
};

fileField.prototype = new jsGrid.Field({ 
    itemTemplate: function (value, item) {
         
        if (typeof file != "undefined") {
            fileToBase64(file, function(e) {
                // use result in callback...
               var base64Data = btoa(e.target.result);
                $("#img" + item.VendorCode).attr("src", 'data:image/jpeg;base64,' + base64Data);
            });
            return '<img id="img' + item.VendorCode + '" src="" height="50" alt="Image preview...">';
        } else 
            if (item.Image.Do == "delete" || item.Image.Name ==null) {   //Если только что удаляли картинку
            return "";
        }
        else if (item.Image != null) {
            return '<img id="img' + item.VendorCode + '" src="'+item.Image.Name+'" height="50" alt="Image preview...">';
        }
      
    },

    editTemplate: function (value, item) {
        if (item.Image.Name == null) {
    
            return $('<form method="POST" id="fileForm" novalidate="novalidate" enctype="multipart/form-data"> <fieldset><div id="imgPreviewСontainer" class="ui compact menu"> <div class="item"> <img id="imgPreview" class="ui Tiny image" src="' + $('#img' + item.VendorCode).attr("src") + '"> <a id="imgPreviewClose" class="floating ui red label">x</a> </div> </div> <input type="file" name="upload" id="uploadFile"/> </fieldset> </form>');
 
             
        }
        return $('<form method="POST" id="fileForm" novalidate="novalidate" enctype="multipart/form-data"> <fieldset><div id="imgPreviewСontainer" class="ui compact menu"> <div class="item"> <img id="imgPreview" class="ui Tiny image" src="' + item.Image.Name + '"> <a id="imgPreviewClose" class="floating ui red label">x</a> </div> </div> <input type="file" name="upload" id="uploadFile"/> </fieldset> </form>');

    },

    editValue: function (value, item) { 
        var files = document.getElementById('uploadFile').files;
        file = files[0];
      //  var fileData = fileToBase64(file);
      //  return $('<img src="'  + '" height="50" alt="Image preview...">');
    }

});

multipleSelectionsField.prototype = new jsGrid.Field({
    itemTemplate: function (value, item) {
        if (item.SizesField != null) {
            return '<p>' + item.SizesField.join() + '</p>';
        } else {
            return "";
        }
    },

    editTemplate: function (value, item) {
        currentDD = dropDownTemplate.clone().dropdown({ allowAdditions: true });;
        for (var i = 0; i < item.SizesField.length; i++) {
            currentDD.append('<a class="ui label transition visible" data-value="' + item.SizesField[i] + '" style="display: inline-block !important;">' + item.SizesField[i] + '<i class="delete icon"></i></a>');
        }
        return currentDD;
    },

    editValue: function (value, item) {
        resultCurrentSizes = [];
        var selectedItems = currentDD.find("a").toArray();
        for (var i = 0; i < selectedItems.length; i++) {
           resultCurrentSizes.push(selectedItems[i].text);
        }
        return resultCurrentSizes;
    }

});

RoleField.prototype = new jsGrid.Field({

    itemTemplate: function (value, item) {
        if (value != null && typeof value != "undefined" && value.length > 30)
            return value.substring(0, 30) + " ...";
        return value;
    },

    insertTemplate: function (value, item) {
    },
   
    editTemplate: function (value, item) {
        if (value == null) {
            value = "";
        }
        return $(' <div class="field">'+
                    '<textarea id="descriptionProduct" rows="2">' + value + '</textarea>' +
                '</div>');
    }
});

$(function () {    
    jsGrid.fields.textarea = RoleField;
    jsGrid.fields.multipleSelections = multipleSelectionsField; 
    jsGrid.fields.file = fileField;

    $("#jsGrid").jsGrid({
        onItemUpdating: function (args) {
            args.item.SizesField = resultCurrentSizes;
            if (isImageDelete) {
                isImageDelete = true;
            }
        },
        height: "100%",
        width: "100%",
        filtering: false,
        inserting: false,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        loadIndication: true,

            pageSize: 10,
            pageButtonCount: 5,
            deleteConfirm: "Вы действительно хотите удалить?",

            controller: dbProducts,
            fields: [
                { name: "Title", type: "text", title: "Название" },
                { name: "CategoryId", type: "select", title: "Категории", items: categories(), valueField: "Id", textField: "Title" },
                { name: "Image", type: "file", width: 100, title: "Изображение" },
                {name: "SizesField", type: "multipleSelections", width: 100, title: "Размер"},
                { name: "Price", type: "text", title: "Цена" },
                    { name: "Description", width: 100, type: "text", title: "Описание" },
                  { name: "VendorCode", type: "text", title: "Артикул" },
                { type: "control" }
            ]
        });
 
    });