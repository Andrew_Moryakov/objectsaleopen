﻿var fullscreenFlag = false;
var buyArray = [];
var productOrign;

var productsToLocalStorage = function (products) {
	buyArray = JSON.stringify(Enumerable.From(products).Where(function (el) { return el.CartCount > 0 }).ToArray());
	localStorage.setItem(productsKey, "");
	localStorage.setItem(productsKey, buyArray);
};
var totalCount = function (products) {
	var r = Enumerable.From(products).Select(function (el) { return el.CartCount }).Sum();
	return r;
};

var setProductsFromCart = function(productsFromCart, products) {
	var newProductList = [];

	for (var i = 0; i < productsFromCart.length; i++) {
			newProductList.push(productsFromCart[i]);
	}

	for (var i = 0; i < products.length; i++) {
		if (!Enumerable.From(newProductList).Any(function (el) { return el.Id === products[i].Id })) {
			newProductList.push(products[i]);
		}

	}

	if (newProductList.length === 0) {
		return products;
	} else {
		return newProductList;
	}
}

var sortById = function (products) {
	return Enumerable.From(products).OrderBy(function (product) { return product.Id }).ToArray();
};

var app = angular.module("app", []);
app.controller("store", function($scope, $http) {
	$scope.cartCount = 0;
	$scope.products = undefined;
	$scope.settingUiStore = settingUiStore;

	var token = getAccessTokenFromLocalStorage();

	$scope.initStore = function ( ) {
		$scope.userName = getUserNameFromToken();

		$scope.PrivateOfficeLink = GetPrivateOfficeLink();

		$http.get("/api/products", {
			headers: { "Authorization": "Bearer " + token }
		}).success(function (data) {

			var sizes = [];

			Enumerable.From(data).ForEach(function (product) {
				Enumerable.From(product.SizesField).ForEach(function (size) {
					if (product.SizesField.length === 1) {
						product.selected = true;
						sizes.push({ "size": size, "selected": true, "cssStyle": "ui green horizontal label" });
					} else {
						sizes.push({ "size": size, "cssStyle": "ui horizontal label" });
					}
				});

				product.SizesField = sizes;
				sizes = [];
			});


			productOrign = data;

			var productsFromCart = JSON.parse(localStorage.getItem(productsKey));
			if (productsFromCart!=null) {
				$scope.cartCount = totalCount(productsFromCart);
				productOrign = setProductsFromCart(productsFromCart, data);
				productOrign = sortById(productOrign);
			}
			$scope.products = productOrign;
		});
	}; 


	$http.get("/api/categories", {
		headers: { "Authorization": "Bearer " + token }
	}).success(function (data) {
		$scope.categories = data;
		});

	$scope.sortByPriceAscending = function() {
		$scope.products = Enumerable.From(productOrign).OrderBy(function(product) { return product.Price }).ToArray();
	};

	$scope.sortByPriceDescending = function() {
		$scope.products = Enumerable.From(productOrign).OrderByDescending(function(product) { return product.Price }).ToArray();
	};

	$scope.productFilterByCategory = function(categoryId) {

		if (typeof categoryId != "undefined") {
			$scope.products = Enumerable.From(productOrign).Where(function(product) { return product.CategoryId == categoryId }).ToArray();
		} else {
			$scope.products = productOrign;
		}
	};

	$scope.sizeSelect = function (style, product) {
		Enumerable.From(product.SizesField).ForEach(function(size) {
			size.cssStyle = "ui horizontal label";
		});

		style.selected = true;
		product.selected = true;
		style.cssStyle = "ui horizontal green label";
	};

	$scope.sizes = function(product) {
		var sizesInDiv = [];
		Enumerable.From(product.SizesField).ForEach(function(el) {
			sizesInDiv.push($('<div style="width: 5px">' + el + "</div>"));
		});
	}; 

	$scope.buy = function (product) {
		if (product.CartCount === 0) {
			product.CartCount++;
			$scope.cartCount++;
		}

		$scope.redirectToCart();
	};

	$scope.addToCart = function(product) {
		product.CartCount++;
		$scope.cartCount++;
		productsToLocalStorage($scope.products);
	};

	$scope.redirectToCart = function() {
		$(".ui.page.dimmer").dimmer("show");
		productsToLocalStorage($scope.products);
		document.location.href = document.location.origin + "/Cart/Index/";
	};
});