﻿function obj_to_query(data) {
    var url = '';
    for (var prop in data) {
        url += encodeURIComponent(prop) + '=' +
            encodeURIComponent(data[prop]) + '&';
    }
    return url.substring(0, url.length - 1);
}

var currentItem;
var clickRoleItem;
$('#gridTab').on('click', '.roleinput', function (val) {
    $(".roleinput").removeAttr("checked");
    $(".roleinput").parent(".slider").attr("class", "ui slider checkbox");
    $(".roleinput").prop("checked", false);

    $(this).prop("checked", true);
    $(this).attr("checked", "checked");
    $(this).parent(".slider").attr("class", "ui slider checkbox checked");
    clickRoleItem = $(this).attr("role");  
});


var uri = '/api/User/';
var db = {
    loadData: function (filter) {
        return $.ajax({
            type: "GET",
            url: uri,
            data: filter,
            dataType: "json"
        });
    },

    insertItem: function (item) {
        return $.ajax({
            type: "POST",
            url: uri,
            data: item,
            dataType: "json"
        });
    },

    updateItem: function (item) {
        return $.ajax({
            type: "PUT",
            url: uri,
            data: item,
            dataType: "json"
        });
    },

    deleteItem: function (item) {
        return $.ajax({
            type: "DELETE",
            url: uri + item.Id,
            dataType: "json"
        });
    }

};
window.db = db;


var RoleField = function (config) {
    jsGrid.Field.call(this, config);
};
RoleField.prototype = new jsGrid.Field({

    itemTemplate: function (value, item) {
        var user = "user";
        var employee = "employee";   
        if (employee === item.Role) {
            item.Role = employee;
            return $("<p>Сотрудник</p>");
        } else
            if (user === item.Role) {
                item.Role = user;
                return $("<p>Клиент</p>");
            }
        return item.Role;
    },

    insertTemplate: function (value, item) {
 return "";
    },
   
    editTemplate: function (value, item) {
       // currentItem = JSON.parse(JSON.stringify(item));
        currentItem = item;
        var empl="", user="";
        if ("employee" === item.Role) {
            empl = "checked='checked'";
        } else
            if ("user" === item.Role) {
                user = "checked='checked'";
            }
      var r = $("<div class='ui form'>" +
                        "<div class='grouped fields'>" +
                        "<div class='field'>" +
                        "<div class='ui slider checkbox'>" +
                        "<input role='employee' class='roleinput'" + empl + " type='radio' id='e" + item.Name + "' name='r" + item.Id + "'>" +
                        "<label>Сотрудник</label>" +
                        "<div class='field'>" +
                        "<div class='ui slider checkbox'>" +
                        "<input role='user' class='roleinput'" + user + " type='radio' id='u" + item.Name + "' name='r" + item.Id + "'>" +
                        "<label>Клиент</label>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>" +
                        "</div>");


        return r;

    },

    //insertValue: function () {
    //    return "66666666666666666666";
    //},

    //editValue: function (value, item) {
    //    return "dfgdfgdfgdfgdfg";
    //}

});
$(function () {    
    jsGrid.fields.role = RoleField;
    $("#jsGrid").jsGrid({
        onItemUpdating: function(args) {
            if (typeof currentItem != "undefined" && typeof clickRoleItem != "undefined") {
                args.item.Role = clickRoleItem;
            }
        },
        height: "100%",
        width: "100%",
        //rowClick: function(args) {
        //    return "<input type='text' value='" + args.item.Name + "'>";
        //},
        //rowRenderer: function (value) {
        //    return "<tr><td>" + value.Name + "</td><td>value.SurName</td><td>value.Adress</td><td>" + value.Email + "</td></tr>";
        //},
        //editRowRenderer: function (value, itemIndex) {
        //    return "<tr><input type='text' value='"+value.Name+"'></p><td>value.SurName</td><td>value.Adress</td><td>" + value.Email + "</td></tr>";
         //},
       
        filtering: false,
        inserting: false,
        editing: true,
        sorting: true,
        paging: true,
        autoload: true,
        loadIndication: true,

            pageSize: 10,
            pageButtonCount: 5,

            deleteConfirm: "Вы действительно хотите удалить?",

            controller: db,

            fields: [
                { name: "Name", type: "text", title: "Имя"},
                { name: "SurName", type: "text", width: 150, title: "Фамилия" },
                { name: "Adress", type: "textarea", width: 150, title: "Адрес" },
                { name: "Email", type: "text", title: "Почта" },
                  { name: "Roles", type: "role", title: "Роль", items: db.colors, valueField: "Color", textField: "Color", width: 100, align: "center" },
                //{ name: "Married", type: "checkbox", title: "Is Married", sorting: false },
                { type: "control" }
            ]
        });
 
    });















//$(document).ready(function () {
//    var crudServiceBaseUrl = "/api/User/",
//        dataSource
//        = new kendo.data.DataSource({
//            transport: {
//                read: {
//                    url: crudServiceBaseUrl,
//                    dataType: "json",
//                    type: "GET"
//                },
//                update: {
//                    url: function (options) {
//                        return crudServiceBaseUrl + "?" + obj_to_query(options.models[0]);
//                    },
//                    dataType: "json",
//                    type: "PUT"
//                },
//                destroy: {
//                    url: function (options) {
//                        return crudServiceBaseUrl + "?" + obj_to_query(options.models[0]);
//                    },
//                    dataType: "json",
//                    type: "DELETE"
//                },
//                create: {
//                    url: crudServiceBaseUrl,
//                    dataType: "json",
//                    type: "POST",
//                    processData: false
//                },
//                parameterMap: function (options, operation) {
//                    if (operation !== "read" && options.models) {
//                        return options;
//                    }
//                }
//            },
//            batch: true,
//            pageSize: 20,
//            schema: {
//                model: {
//                    id: "Id",
//                    fields: {
//                        Id: {
//                            editable: false,
//                            nullable: true
//                        },
//                        Name: {
//                            type: "string"
//                        },
//                        SurName: {
//                            type: "string"
//                        },
//                        Adress: {
//                            type: "string"
//                        },
//                        Email: {
//                            type: "string"
//                        }
//                    }
//                }
//            }
//        });

//    $("#grid").kendoGrid({
//        dataSource: dataSource,
//        pageable: true,
//        groupable: true,
//        sortable: true,
//        //height: 550,
//        //toolbar: ["create"],
//        columns: [
//            //"Id",
//            {
//                field: "Name",
//                title: "Имя",
//                width: "120px"
//            }, {
//                field: "SurName",
//                title: "Фамлия",
//                width: "120px"
//            }, {
//                field: "Adress",
//                title: "Адрес",
//                width: "120px"
//            }, {
//                field: "Email",
//                title: "Почта",
//                width: "120px"
//            }, {
//                command: ["edit", "destroy"],
//                title: "&nbsp;",
//                width: "250px"
//            }
//        ],
//        editable: "inline"
//    });
//});







//$(document).ready(function () {

//    var crudServiceBaseUrl = "/api/User/",
//        dataSource = new kendo.data.DataSource({
//            transport: {
//                read: function (options) {
//                    $.ajax({
//                        url: crudServiceBaseUrl,
//                        dataType: "json", 
//                        type: "GET",
//                        success: function (data) {
//                            $("#grid").data('kendoGrid').dataSource.data(data);
//                        }
//                    });
//                },
//                update: {
//                    url: function (options) {
//                        return crudServiceBaseUrl + "?" + obj_to_query(options.models[0]);
//                    },
//                    dataType: "json",
//                    type: "PUT"
//                },
//                destroy: {
//                    url: function (options) {
//                        return crudServiceBaseUrl + "?" + obj_to_query(options.models[0]);
//                    },
//                    dataType: "json",
//                    type: "DELETE"
//                },
//                create: {
//                    url: function (options) {
//                        return crudServiceBaseUrl + "?" + obj_to_query(options.models[0]);
//                    },
//                    dataType: "json",
//                    type: "POST"
//                },
//                parameterMap: function (options, operation) {
//                    if (operation !== "read" && options.models) {
//                        return options;
//                        //} else if (operation === "create" && options.models) {
//                        //    return { models: kendo.stringify(options.models) };
//                    }
//                }
//            },
               
//            batch: true,
//            pageSize: 20,
//            schema: {
//                model: {
//                    id: "Id",
//                    fields: {
//                        Id: {
//                            editable: false,
//                            nullable: true
//                        },
//                        Name: {
//                            type: "string"
//                        },
//                        SurName: {
//                            type: "string"
//                        },
//                        Adress: {
//                            type: "string"
//                        },
//                        Email: {
//                            type: "string"
//                        },
//                        //RoleEmployee: {
//                        //    type: "boolean",
//                        //  //  editable: false,
//                        //},
//                        //RoleUser: {
//                        //    type: "boolean",
//                        //   // editable: false,
//                        //}
//                        //RoleName: { 
//                        //}
//                    }
//                }
//            }
//        });

//    $("#grid").kendoGrid({
//        dataSource: dataSource,
//        columns: [
//            //"Id",
//            {
//                field: "Name",
//                title: "Имя",
//                width: "120px"
//            }, {
//                field: "SurName",
//                title: "Фамлия",
//                width: "120px"
//            }, {
//                field: "Adress",
//                title: "Адрес",
//                width: "120px"
//            }, {
//                field: "Email",
//                title: "Почта",
//                width: "120px"
//            },
//            //{
//            //    field: "RoleEmployee",
//            //    title: "Роль сотрудник",
//            //    //editor: numericEditor
//            //    template: "#= myFunc(data) #"
//            //    //template:
//            //    //    "<div  class='ui form'>"+
//            //    //        "<div class='grouped fields'>"+
//            //    //            "<div class='field'>"+
//            //    //                "<div class='ui slider checkbox'>"+
//            //    //                    "<input type='radio' name='#=Id#'>"+
//            //    //                    "<label>Сотрудник</label>"+
//            //    //                "</div>"+
//            //    //            "</div>"+
//            //    //        "</div>"+
//            //    //   "</div>"

//            //},
//            //{
//            //    field: "RoleUser",
//            //    title: "Роль клиент",
//            //    template:
//            //        "<div  class='ui form'>" +
//            //            "<div class='grouped fields'>"+
//            //                "<div class='field'>"+
//            //                    "<div class='ui slider checkbox'>"+
//            //                        "<input type='radio' name='#=Id#' checked='checked'>" +
//            //                        "<label>Клиент</label>"+
//            //                    "</div>"+
//            //                "</div>"+
//            //            "</div>"+
//            //       "</div>"
//            //},
////            {
////                field: "RoleName",
////                editor: categoryDropDownEditor,
////                template: "#=RoleName#"
////},
//        {
//                command: ["edit", "destroy"],
//                title: "&nbsp;",
//                width: "250px"
//            }
//        ],
     
//        pageable: true,
//        groupable: true,
//        sortable: true,
//        //height: 550,
//       toolbar: ["create", "save", "cancel"],
//        editable:"inline"
//        //editable: {
//        //    update: true,
//        //    create: true
//        //}
//    });
//});

////function definition
//function myFunc(model) {
//    if (model.isValid) {
//        return "<span style='color: green'>Foo::" + model.foo + " is valid! </span>"
//    } else {
//        return "not valid"
//    }
//}
////function categoryDropDownEditor(container, options) {

////    $("<div class='ui form'>" +
////            "<div class='grouped fields'>" +
////            "<div class='field'>" +
////            "<div class='ui slider checkbox'>" +
////            "<input type='radio' id='c" + options.model.Id + "' name='" + options.model.Id + "' checked='checked'>" +
////            "<label>Клиент</label>" +
////            "<div class='field'>" +
////            "<div class='ui slider checkbox'>" +
////            "<input id='e#=Id#' type='radio' id='e" + options.model.Id + "' name='" + options.model.Id + "'>" +
////            "<label>Сотрудник</label>" +
////            "</div>" +
////            "</div>" +
////            "</div>" +
////            "</div>" +
////            "</div>" +
////            "</div>")
////        .appendTo(container);
////    //rolename = $("#" + options.model.Id).next().text();
////    //if ($("#c" + options.model.Id).attr("checked") == "checked") {
////    //    rolename = "client";
////    //}
////    //if ($("#e" + options.model.Id).attr("checked") == "checked") {
////    //    rolename = "employee";
////    //}
////    //  options.model.RoleName = $("#c" + options.model.Id).next().text();
////}