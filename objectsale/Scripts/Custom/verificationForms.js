﻿
var formreg = false;
$(document).ready(function () {
    $(".selector").validate({
        onfocusout: true,
        onkeyup: true,
        onclick: true,
        onsubmit: true,
    });
    formreg = $('#form1');
    formreg.validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            adress: {
                required: true
            },
            name: {
                required: true
            },
            surName: {
                required: true
            },
            password: {
                minlength: 5,
                required: true
            },
            passwordConfirm: {
                minlength: 5,
                required: true,
                equalTo: "#password"
            }
        },
        messages: {
            email: {
                email: "Вы должны ввести Вашу почту!",
                required: "Вы ввели не корректную почту"
            },
            adress: {
                required: "Введите Ваш адрес"
            },
            name: {
                required: "Введите Ваше имя"
            },
            surName: {
                required: "Введите Вашу фамилию"
            },
            password: {
                minlength: "Пароль должен содержать не менее 6 символов",
                required: "Введите Ваш пароль"
            },
            passwordConfirm: {
                minlength: "Пароль должен содержать не менее 6 символов",
                required: "Введите пароль ещё раз",
                equalTo: "Пароли должны совпадать"
            }
        }
    });

    $('#form2').validate({
        rules: {
            email: {
                email: true,
                required: true
            },
            password: {
                minlength: 5,
                required: true
            }
        },
        messages: {
            email: {
                email: "Вы должны ввести Вашу почту!",
                required: "Вы ввели не корректную почту"
            },
            password: {
                minlength: "Пароль должен содержать не менее 6 символов",
                required: "Введите Ваш пароль"
            }
        }
    });
});