﻿			$.ajax({
				type: "GET",
				url: visitUrl,
				dataType: "json",
				success: function (visits) {
					setParams(visits.visitCounts, visits.visitDates);

				},
				beforeSend: function (xhr) {
					xhr.setRequestHeader("Authorization", "Bearer " + getAccessTokenFromLocalStorage());
				}
			});

			var data = {};
			var currentChart;

			$('body').on('click', '#chartTab', function (val) {
				drawChart();
				$(window).resize(function () {
					if (currentChart) { 
						setChartDimensions();
						currentChart.stop();
						currentChart.resize(currentChart.render, true);
					}
				});
			});


			function setChartDimensions() {
				var width = $("section").width(),
					height = $("footer").offset().top - $("canvas").offset().top - 20;
				if (currentChart) {
					currentChart.chart.aspectRatio = width / height;
				} else {
					$("canvas").attr("width", width);
					$("canvas").attr("height", height);
				}
			}

			function setParams(visitCounts, visitDates) {
				data = {
					labels: visitDates,
					datasets: [
						{
							label: "My First dataset",
							fillColor: "rgba(220,0,220,0.2)",
							strokeColor: "rgba(220,220,220,1)",
							pointColor: "rgba(220,220,220,1)",
							pointStrokeColor: "#fff",
							pointHighlightFill: "#fff",
							pointHighlightStroke: "rgba(220,220,220,1)",
							data: visitCounts
						}
					]
				};
			}

			function drawChart() {
				
				setChartDimensions();
				var ctx = $("canvas")[0].getContext("2d");
				currentChart = new Chart(ctx).Line(data, { animation: false, responsive: false });
			}