﻿var visitKey = 'visitKey';
var visitValue = 'visit';


$(document).ready(function () {
	var data = sessionStorage.getItem(visitKey);

	if (data !== visitValue) {
		sessionStorage.setItem(visitKey, visitValue);

		var userName = getUserNameFromToken();
		var dataForVisit= {
			User: { UserName: userName }
		}

		HttpPostJqAjax(visitUrl, true, dataForVisit, true);//Отправляем информацию о посеении
	}
});
